﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parking.Vehicles
{
    public abstract class BaseVehicle
    {
        public string Id { get; private set; }
        public bool Parked { get; set; }
        protected decimal PriceRatio;

        public VehicleType Type { get; protected set; }

        public decimal Balance { get; protected set; }
        public BaseVehicle(string id, decimal balance)
        {
            Id = id;
            Balance = balance;
        }

        public decimal AddFunds(decimal amount)
        {
            this.Balance += amount;
            return this.Balance;
        }

        public Transaction Withdraw()
        {
            var amount = PriceRatio * Settings.Price;
            if (this.Balance < amount)
            {
                amount *= Settings.FactorFine;
            }
            this.Balance -= amount;
            return new Transaction(Id, amount);
        }
    }
}
