﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parking.Vehicles
{
    public class Motorbike : BaseVehicle
    {
        public Motorbike(string id, decimal balance) : base(id, balance)
        {
            PriceRatio = 1;
            Type = VehicleType.Motorbike;
        }
        public override string ToString()
        {
            return $"{Id} - Motorbike";
        }
    }
}
