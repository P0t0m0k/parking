﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parking.Vehicles
{
    public class Bus : BaseVehicle
    {
        public Bus(string id, decimal balance) : base(id, balance)
        {
            PriceRatio = 3.5m;
            Type = VehicleType.Bus;
        }
        public override string ToString()
        {
            return $"{Id} - Bus";
        }
    }
}
