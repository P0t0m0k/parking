﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parking.Vehicles
{
    public class Truck : BaseVehicle
    {
        public Truck(string id, decimal balance) : base(id, balance)
        {
            PriceRatio = 5;
            Type = VehicleType.Truck;
        }

        public override string ToString()
        {
            return $"{Id} - Truck";
        }
    }
}
