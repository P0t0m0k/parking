﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parking.Vehicles
{
    public enum VehicleType
    {
        Car = 1,
        Truck = 2,
        Bus = 3,
        Motorbike = 4
    }
}
