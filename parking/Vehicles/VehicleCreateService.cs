﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parking.Vehicles
{
    public class VehicleCreateService
    {
        public static BaseVehicle Create(string id, VehicleType type, decimal balance)
        {
            switch (type)
            {
                case VehicleType.Bus:
                    return new Bus(id, balance);
                case VehicleType.Car:
                    return new Car(id, balance);
                case VehicleType.Motorbike:
                    return new Motorbike(id, balance);
                case VehicleType.Truck:
                    return new Truck(id, balance);
                default:
                    throw new Exception();
            }
        }
    }
}
