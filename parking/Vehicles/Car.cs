﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parking.Vehicles
{
    public class Car : BaseVehicle
    {
        public Car(string id, decimal balance) : base(id, balance)
        {
            PriceRatio = 2;
            Type = VehicleType.Car;
        }

        public override string ToString()
        {
            return $"{Id} - Car";
        }
    }
}
