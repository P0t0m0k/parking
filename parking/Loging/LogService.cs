﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Parking
{
    public class LogService
    {
        private string _fileName;

        public LogService(string fileName)
        {
            _fileName = fileName;
        }

        public void Write(string[] values)
        {
            using (var fileStream = File.AppendText(_fileName))
            {

                foreach (var value in values)
                {
                    fileStream.WriteLine(value);
                }
            }
        }

        public string[] ReadAll()
        {
            return File.ReadAllLines(_fileName);
        }  


    }
}
