﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parking.Interfaces
{
    public interface IUiService
    {
        void Run();
    }
}
