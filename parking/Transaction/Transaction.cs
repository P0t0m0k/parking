﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parking
{
    public class Transaction
    {
        public DateTime Created { get; private set; }
        public string VehicleId { get; private set; }
        public decimal Amount { get; private set; }

        public Transaction(string vehicleId, decimal amount)
        {
            Created = DateTime.Now;
            VehicleId = vehicleId;
            Amount = amount;
        }

        public override string ToString()
        {
            return $"Created:{Created}, VehicleId:{VehicleId}, Amount:{Amount} ";
        }
    }
}
