﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;

namespace Parking
{
    public static class TransactionLogService
    {
        private readonly static LogService _log = new LogService(@"Transactions.log");

        public static void AddToLog(List<Transaction> transactions)
        {
            _log.Write(transactions.Select(x => x.ToString()).ToArray());
        }

        public static void ReadLogToConsole()
        {
            foreach (var item in _log.ReadAll())
            {
                Console.WriteLine(item);
            }
        }
        public static string[] ReadLog()
        {
            return  _log.ReadAll();
        }
    }
}
