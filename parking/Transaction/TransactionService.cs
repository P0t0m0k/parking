﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;

namespace Parking
{
    public class TransactionService : IDisposable
    {

        private Timer _payTimer;
        private Timer _logTimer;

        private readonly List<Transaction> _transactions = new List<Transaction>();

        public decimal Balance { get; private set; }

        public event Func<Transaction> Withdrawn;

        public TransactionService()
        {
            _payTimer = new Timer(Settings.PeriodPayments.TotalMilliseconds);
            _payTimer.Elapsed += _payTimer_Elapsed;
            _payTimer.AutoReset = true;
            _payTimer.Enabled = true;

            _logTimer = new Timer(60*1000);
            _logTimer.Elapsed += _logTimer_Elapsed;
            _logTimer.AutoReset = true;
            _logTimer.Enabled = true;

            Balance = Settings.InitBalance;
        }

        private void _logTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            var temp = _transactions.ToArray();
            _transactions.Clear();

           TransactionLogService.AddToLog(temp.ToList());
        }

        private void _payTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (Withdrawn != null)
            {
                foreach (var item in Withdrawn.GetInvocationList())
                {
                    var transaction = item.DynamicInvoke() as Transaction;
                    Balance += transaction.Amount;
                    this._transactions.Add(transaction);
                }
            }
        }

        public decimal GetSumFromLastMinute() {
            return _transactions.Select(x => x.Amount).Sum();
        }


        public List<Transaction> GetTransactionFromLastMinute() {
            return _transactions;
        }

        public void Dispose()
        {
            _payTimer.Stop();
            _payTimer.Dispose();
        }
    }
}
