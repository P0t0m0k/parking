﻿using Parking.Interfaces;
using System;
using System.Collections.Generic;

namespace Parking
{
    class Program
    {
        private static readonly IUiService _ui = new ConsoleUiService();


        static void Main(string[] args)
        {
            _ui.Run();
        }


    }
}
