﻿using Parking.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Parking
{

    public class Parking 
    {

        private static readonly Parking instance = new Parking();

        private readonly List<BaseVehicle> _vehicles = new List<BaseVehicle>();

        private TransactionService _transactionService = new TransactionService();

        private Parking()
        {
        }


        public static Parking getInstance()
        {
            return instance;
        }


        public void AddVehicle(BaseVehicle vehicle)
        {
            if (_vehicles.Any(x => x.Id.ToLower() == vehicle.Id.ToLower()))
                throw new Exception("Has same id");
            _vehicles.Add(vehicle);
        }

        public void RemoveVehicle(BaseVehicle vehicle)
        {
            _vehicles.Remove(vehicle);
        }

        public BaseVehicle FindVehicleById(string id)
        {
            return _vehicles.FirstOrDefault(x => x.Id.ToLower() == id.ToLower());
        }

        public BaseVehicle[] GetAllVehicle()
        {
            return _vehicles.ToArray();
        }

        public void PutOnParking(BaseVehicle vehicle)
        {
            _transactionService.Withdrawn += vehicle.Withdraw;
            vehicle.Parked = true;
        }

        public void TakeWithParking(BaseVehicle vehicle)
        {
            _transactionService.Withdrawn -= vehicle.Withdraw;
            vehicle.Parked = false;
        }

        public int GetAllParkingSpaces()
        {
            return Settings.MaxCapacity;
        }
        public int GetBusyParkingSpaces()
        {
            return _vehicles.Count(x => x.Parked);
        }

        public decimal GetBalance() {
            return _transactionService.Balance;
        }

        public decimal GetSumFromLastMinute() {
            return _transactionService.GetSumFromLastMinute();
        }
        public List<Transaction> GetTransactionFromLastMinute()
        {
            return _transactionService.GetTransactionFromLastMinute();
        }

    }
}
