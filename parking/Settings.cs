﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parking
{
    public class Settings
    {
        public static decimal InitBalance { get; set; } = 0;

        public static int MaxCapacity { get; set; } = 10;

        public static TimeSpan PeriodPayments { get; set; } = new TimeSpan(0, 0, 5);

        public static decimal FactorFine { get; set; } = 2.5m;

        public static decimal Price { get; set; } = 1;
    }
}
