﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingClient.Models
{
    public class TransactionModel
    {
        public DateTime Created { get;  set; }
        public string VehicleId { get;  set; }
        public decimal Amount { get;  set; }

        public override string ToString()
        {
            return $"Created:{Created}, VehicleId:{VehicleId}, Amount:{Amount} ";
        }
    }
}
