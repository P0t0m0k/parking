﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingClient.Models
{
    public class VehicleModel
    {
        public string Id { get; set; }
        public VehicleType Type {get; set; }

        public decimal Balance { get; set; }

        public override string ToString()
        {
            return $"{Id} - {Type}"; ;
        }
    }
}
