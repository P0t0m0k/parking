﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingClient.Interfaces
{
    public interface IUiService
    {
        void Run();
    }
}
