﻿using Newtonsoft.Json;
using ParkingClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Transactions;

namespace ParkingClient
{

    public class Parking : IDisposable
    {

        private static readonly Parking instance = new Parking();

        private HttpClient _client;

        private Parking()
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri("https://localhost:44314/api/");
        }


        public static Parking getInstance()
        {
            return instance;
        }


        public void AddVehicle(VehicleModel vehicle)
        {
            using (var requestContent = new StringContent(JsonConvert.SerializeObject(vehicle), Encoding.UTF8, "application/json"))
            {
                using (var req = new HttpRequestMessage(HttpMethod.Post, "vechicles"))
                {
                    req.Content = requestContent;
                    _client.SendAsync(req).Wait();
                }
            }
        }

        public void RemoveVehicle(VehicleModel vehicle)
        {

            using (var req = new HttpRequestMessage(HttpMethod.Delete, "vechicles"))
            {
                _client.SendAsync(req).Wait();
            }
        }

        public VehicleModel FindVehicleById(string id)
        {
            var result = _client.GetStringAsync($"vechicles/{id}").Result;
            return JsonConvert.DeserializeObject<VehicleModel>(result);
        }

        public VehicleModel[] GetAllVehicle()
        {
            var result = _client.GetStringAsync("vechicles").Result;
            return JsonConvert.DeserializeObject<VehicleModel[]>(result);
        }

        public void PutOnParking(VehicleModel vehicle)
        {
            using (var requestContent = new StringContent(JsonConvert.SerializeObject(new { VehicleId = vehicle.Id, IsParked = true }), Encoding.UTF8, "application/json"))
            {
                using (var req = new HttpRequestMessage(HttpMethod.Post, "parking"))
                {
                    req.Content = requestContent;
                    _client.SendAsync(req).Wait();
                }
            }
        }

        public void TakeWithParking(VehicleModel vehicle)
        {
            using (var requestContent = new StringContent(JsonConvert.SerializeObject(new { VehicleId = vehicle.Id, IsParked = false }), Encoding.UTF8, "application/json"))
            {
                using (var req = new HttpRequestMessage(HttpMethod.Post, "parking"))
                {
                    req.Content = requestContent;
                    _client.SendAsync(req).Wait();
                }
            }
        }

        public (int all, int busy) GetInfoParkingSpaces()
        {

            var result = _client.GetStringAsync("parking/info-space").Result;
            return JsonConvert.DeserializeObject<(int all, int busy)>(result);
        }

        public string GetBalance()
        {
            var result = _client.GetStringAsync("parking/balance").Result;
            return result;
        }

        public decimal AddFundsForVehicleById(string id, decimal amount)
        {
            VehicleModel vehicle;
            using (var requestContent = new StringContent(amount.ToString(), Encoding.UTF8, "application/json"))
            {
                using (var req = new HttpRequestMessage(HttpMethod.Put, $"vechicles/{id}"))
                {
                    req.Content = requestContent;
                    using (var response = _client.SendAsync(req).Result)
                    {
                        response.EnsureSuccessStatusCode();
                        vehicle = JsonConvert.DeserializeObject<VehicleModel>(response.Content.ReadAsStringAsync().Result);
                    }
                }
            }

            return vehicle.Balance;
        }

        public string GetSumFromLastMinute()
        {
            var result = _client.GetStringAsync("parking/stats-last-minute").Result;
            return result;
        }
        public List<TransactionModel> GetTransactionFromLastMinute()
        {
            var result = _client.GetStringAsync("parking/transaction-last-minute").Result;
            return JsonConvert.DeserializeObject<List<TransactionModel>> (result);
        }


        public string[] GetTransactionFromLog()
        {
            var result = _client.GetStringAsync("parking/transaction-log").Result;
            return JsonConvert.DeserializeObject<string[]>(result);
        }
        public void Dispose()
        {
            _client.Dispose();
        }
    }
}
