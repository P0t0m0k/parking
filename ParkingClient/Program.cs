﻿using ParkingClient.Interfaces;
using System;

namespace ParkingClient
{
    class Program
    {

        private static readonly IUiService _ui = new ConsoleUiService();
        static void Main(string[] args)
        {
            _ui.Run();
        }
    }
}
