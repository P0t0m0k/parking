﻿using ParkingClient.Interfaces;
using ParkingClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace ParkingClient
{
    public class ConsoleUiService : IUiService
    {
        private static Dictionary<int, (string title, Action action)> _actions = new Dictionary<int, (string title, Action action)>(){
            {1, (title: "Створити/поставити Транспортний засіб на Парковку", action: ConsoleUiService.AddPutVehicle )},
            {2, (title: "Видалити/забрати Транспортний засіб з Парковки", action: RemoveTakeVehicle )},
            {3, (title: "Дізнатися поточний баланс Парковки", action: GetBalance )},
            {4, (title: "Суму зароблених коштів за останню хвилину", action: GetBalance )},
            {5, (title: "Дізнатися кількість вільних/зайнятих місць на парковці", action: GetParkingPlace )},
            {6, (title: "Вивести на екран усі Транзакції Праковки за останню хвилину", action: GetTransactionFromLastMinute )},
            {7, (title: "Вивести усю історію Транзакцій", action: GetAllTransaction )},
            {8, (title: "Вивести всі транспортні засоби", action: GetAllVehicles )},
            {9, (title: "Поповнити баланс транспортного засобу", action: AddFunds )},
            {0, (title: "Exit", action:() => { Console.Write("Exit"); Thread.Sleep(1000);}) }
        };

        private readonly static Parking _parking = Parking.getInstance();


        public void Run()
        {

            Console.OutputEncoding = System.Text.Encoding.UTF8;
            ShowMenu();
        }

        public static void ShowMenu()
        {
            Console.WriteLine("Виберіть дію:");
            //Console.WriteLine("1 - Add Car, 2 - Remove Car, 3 - Get Balance, 4 - Get sum from last minute, 5 - Get Parking place, 6 - Get transaction from last minutes, 7 - Get all transaction, 8 - Get all cars, 9 - add funds");

            Console.WriteLine(string.Join("\r\n", _actions.Select((x) => $"{x.Key} - {x.Value.title}")));

            var key = Console.ReadKey();
            Console.WriteLine();
            if (int.TryParse(key.KeyChar.ToString(), out int selectValue))
            {
                if (_actions.ContainsKey(selectValue))
                {
                    _actions[selectValue].action();
                }
            }
        }

        public static void AddPutVehicle()
        {
            Console.Clear();
            if (_parking.GetAllVehicle().Any())
            {
                Console.WriteLine("Виберіть дію: 1 - Додати транспортний засіб, 2 - Поставити на парковку");
                var key = Console.ReadKey();
                Console.Write("\b");
                if (int.TryParse(key.KeyChar.ToString(), out var selected))
                {
                    switch (selected)
                    {
                        case 1:
                            AddVehicle();
                            return;
                        case 2:
                            PutOnParking();
                            return;
                    }
                }
                AddPutVehicle();
            }
            else
            {
                AddVehicle();
            }
        }

        public static void AddVehicle()
        {
            Console.Clear();

            Console.WriteLine("Ідентифікатор:");
            string id;
            while (true)
            {
                id = Console.ReadLine().Trim();
                if (!string.IsNullOrWhiteSpace(id))
                {
                    break;
                }
                Console.WriteLine("Ідентифікатор не може бути пустим ...");
            }

            Console.WriteLine("Тип транспортного засібу:");
            Console.WriteLine("1 - Легкова; 2 - Вантажна; 3 - Автобус; 4 - Мотоцикл;");
            VehicleType type;

            while (!Enum.TryParse<VehicleType>(Console.ReadLine().Trim(), out type) || !Enum.IsDefined(typeof(VehicleType), type))
            {
                Console.WriteLine("Повторіть вибір типу тарнспортного засобу...");
            };

            Console.WriteLine("Встановити баланс: (за замовчуванням: 100)");
            decimal balance;
            string balanceString;
            do
            {
                balanceString = Console.ReadLine();
                if (string.IsNullOrWhiteSpace(balanceString))
                {
                    balance = 100m;
                    break;
                }
                else if (decimal.TryParse(balanceString, out balance))
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Повторіть ввід ...");
                }
            } while (true);

            var vehicle = new VehicleModel { Id = id, Type = type, Balance = balance }; /*VehicleCreateService.Create(id, type, balance);*/
            try
            {
                _parking.AddVehicle(vehicle);
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"Створено: {vehicle}");
                Console.ResetColor();
            }
            catch (Exception e)
            {
                Console.WriteLine($"Виникла помилка: {e.Message}");
            }


            ShowMenu();
        }

        public static void GetAllVehicles()
        {
            Console.Clear();
            var allVehicles = _parking.GetAllVehicle();

            if (allVehicles.Length == 0)
            {
                Console.WriteLine($"No Vehicle");
            }

            foreach (var item in allVehicles)
            {
                Console.WriteLine($"{item}");
            }
            Console.ReadKey();

            ShowMenu();
        }


        private static VehicleModel FoundVehicle()
        {
            Console.WriteLine("Введіть ідентифікатор для пошуку транспортного засобу:");

            string id;
            do
            {
                id = Console.ReadLine().Trim();
                if (!string.IsNullOrWhiteSpace(id))
                {
                    break;
                }
                Console.WriteLine("Ідентифікатор не може бути пустим ...");
            }
            while (true);
            var selected = _parking.FindVehicleById(id);
            if (selected == null)
            {
                Console.WriteLine($"Транспортний засіб не знайденно...");
            }
            else
            {
                Console.WriteLine($"Знайдено: {selected}");
            }
            return selected;
        }


        public static void RemoveTakeVehicle()
        {
            Console.Clear();

            if (_parking.GetAllVehicle().Any())
            {

                Console.WriteLine("Виберіть дію: 1 - Видалити транспортний засіб, 2 - Забрати з парковки");
                var key = Console.ReadKey();

                Console.Write("\b");

                if (int.TryParse(key.KeyChar.ToString(), out var selected))
                {
                    switch (selected)
                    {
                        case 1:
                            RemoveVehicle();
                            return;
                        case 2:
                            TakeWithParking();
                            return;
                    }
                }
                RemoveTakeVehicle();
            }
            else
            {

                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Ви не маєте транспортних засобів ...");
                Console.ResetColor();
                ShowMenu();
            }
        }
        public static void RemoveVehicle()
        {
            VehicleModel selected;
            do
            {
                selected = FoundVehicle();
                if (selected == null)
                {
                    Console.WriteLine("Провести пошук повторно? (Y/N)");
                    var answerT = Console.ReadLine();

                    if (answerT.Trim().ToLower() != "y")
                    {
                        Console.Clear();
                        ShowMenu();
                        return;
                    }
                }

            } while (selected == null);

            Console.WriteLine("Ви бажаете видалити знайдений транспортній засіб? (Y/N)");

            var answer = Console.ReadLine();
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            if (answer.Trim().ToLower() == "y")
            {
                _parking.RemoveVehicle(selected);
                Console.WriteLine($"{selected}: Видалено");
            }
            else
            {
                Console.WriteLine($"{selected}: Не видалено");
            }
            Console.ResetColor();
            ShowMenu();
        }

        public static void PutOnParking()
        {
            VehicleModel selected;
            do
            {
                selected = FoundVehicle();
                if (selected == null)
                {
                    Console.WriteLine("Провести пошук повторно? (Y/N)");
                    var answerT = Console.ReadLine();

                    if (answerT.Trim().ToLower() != "y")
                    {
                        Console.Clear();
                        ShowMenu();
                        return;
                    }
                }

            } while (selected == null);

            Console.WriteLine("Встановити транспортний засіб на парковку? (Y/N)");

            var answer = Console.ReadLine();
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            if (answer.Trim().ToLower() == "y")
            {
                _parking.PutOnParking(selected);
                Console.WriteLine($"{selected}: Запаркована");
            }
            else
            {
                Console.WriteLine($"{selected}: Не запаркована");
            }
            Console.ResetColor();
            ShowMenu();
        }

        public static void TakeWithParking()
        {
            if (_parking.GetInfoParkingSpaces().busy > 0)
            {
                VehicleModel selected;
                do
                {
                    selected = FoundVehicle();
                    if (selected == null)
                    {
                        Console.WriteLine("Провести пошук повторно? (Y/N)");
                        var answerT = Console.ReadLine();

                        if (answerT.Trim().ToLower() != "y")
                        {
                            Console.Clear();
                            ShowMenu();
                            return;
                        }
                    }

                } while (selected == null);

                Console.WriteLine("Забрати транспортний засіб з парковки? (Y/N)");

                var answer = Console.ReadLine();
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Green;

                if (answer.Trim().ToLower() == "y")
                {
                    _parking.TakeWithParking(selected);
                    Console.WriteLine($"{selected}: Транспортний засіб забраний");
                }
                else
                {
                    Console.WriteLine($"{selected}: Транспортний засіб не забраний");
                }

            }
            else
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"Не має припакованих транспортних засобів");
            }
            Console.ResetColor();
            ShowMenu();
        }


        public static void GetBalance()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"Баланс: {_parking.GetBalance()};");
            Console.ResetColor();
            Console.WriteLine();
            ShowMenu();
        }

        public static void GetSumFromLastMinute()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"Суму зароблених коштів за останню хвилину: {_parking.GetSumFromLastMinute()};");
            Console.ResetColor();
            Console.WriteLine();
            ShowMenu();
        }

        public static void GetParkingPlace()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            var info = _parking.GetInfoParkingSpaces();
            Console.WriteLine($"Вільних місць: {info.all - info.busy};");
            Console.WriteLine($"Занятих місць: {info.busy};");
            Console.ResetColor();
            Console.WriteLine();
            ShowMenu();
        }

        public static void GetTransactionFromLastMinute()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            foreach (var item in _parking.GetTransactionFromLastMinute())
            {
                Console.WriteLine(item);
            }
            Console.ResetColor();
            Console.WriteLine();
            ShowMenu();
        }

        public static void GetAllTransaction()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;

            _parking.GetTransactionFromLog();
            foreach (var item in _parking.GetTransactionFromLog())
            {
                Console.WriteLine(item);
            }

            //TransactionLogService.ReadLogToConsole();

            foreach (var item in _parking.GetTransactionFromLastMinute())
            {
                Console.WriteLine(item);
            }

            Console.ResetColor();
            Console.WriteLine();
            ShowMenu();
        }

        public static void AddFunds()
        {
            VehicleModel selected;
            do
            {
                selected = FoundVehicle();
                if (selected == null)
                {
                    Console.WriteLine("Провести пошук повторно? (Y/N)");
                    var answerT = Console.ReadLine();

                    if (answerT.Trim().ToLower() != "y")
                    {
                        Console.Clear();
                        ShowMenu();
                        return;
                    }
                }

            } while (selected == null);

            Console.WriteLine("Поповнити баланс знайденому транспортному засібу? (Y/N)");

            var answer = Console.ReadLine();

            if (answer.Trim().ToLower() == "y")
            {
                Console.WriteLine("Введіть суму поповнення рахунку?");

                decimal amount;
                while (!decimal.TryParse(Console.ReadLine().Trim(), out amount))
                {
                    Console.WriteLine("Повторно введіть суму поповнення ...");
                }
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Green;
                var newBalance =_parking.AddFundsForVehicleById(selected.Id, amount);
                Console.WriteLine($"Рахунок поповненно на {amount}, зараз на рахунку {newBalance}");
            }
            Console.ResetColor();
            ShowMenu();
        }
    }
}
