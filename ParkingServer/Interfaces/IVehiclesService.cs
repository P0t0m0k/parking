﻿using Parking.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParkingServer.Interfaces
{
    public interface IVehiclesService
    {
        Parking.Vehicles.BaseVehicle[] GetAll();
        Parking.Vehicles.BaseVehicle GetById(string id);

        void Create(string id, VehicleType type, decimal balance );

        void Delete(string id);
    }
}
