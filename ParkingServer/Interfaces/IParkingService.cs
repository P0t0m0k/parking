﻿using Parking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParkingServer.Interfaces
{
    public interface IParkingService
    {
        void AddVehicleOnParking(string idVehicle);
        void TakeVehicleWithParking(string idVehicle);
        int GetAllParkingSpaces ();
        int GetBusyParkingSpaces ();
        decimal GetBalance();
        decimal GetSumFromLastMinute();

        List<Transaction> GetTransactionFromLastMinute();

        string [] GetTransactionFromLog();
    }
}
