﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ParkingServer.Interfaces;
using ParkingServer.Model;

namespace ParkingServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VechiclesController : ControllerBase
    {
        private readonly IVehiclesService _vehicles;

        public VechiclesController(IVehiclesService vehicles)
        {
            _vehicles = vehicles;
        }
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_vehicles.GetAll());
        }

        [HttpGet("{id}")]
        public IActionResult Get(string id)
        {
            return Ok(_vehicles.GetById(id));
        }

        [HttpPost]
        public IActionResult Post(VehicleModel model)
        {
            _vehicles.Create(model.Id, model.Type, model.Balance);

            return Ok();
        }

        [HttpPut("{id}")]
        public IActionResult UpdateBalance(string id, [FromBody] decimal amount)
        {
            var vehicle = _vehicles.GetById(id);
            vehicle.AddFunds(amount);

            return Ok(vehicle);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(string id) {

            _vehicles.Delete(id);
            return Ok();
        }
    }
}