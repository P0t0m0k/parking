﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ParkingServer.Interfaces;
using ParkingServer.Model;

namespace ParkingServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private readonly IParkingService _parkingService;


        public ParkingController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet("info-space")]
        public IActionResult GetParkingInfo()
        {
            return Ok((all: _parkingService.GetAllParkingSpaces(), busy: _parkingService.GetBusyParkingSpaces()));
        }

        [HttpGet("stats-last-minute")]
        public IActionResult GetSumFromLastMinute()
        {

            _parkingService.GetSumFromLastMinute();
            return Ok();
        }

        [HttpGet("transaction-last-minute")]
        public IActionResult GetTransactionFromLastMinute()
        {
            return Ok(_parkingService.GetTransactionFromLastMinute());
        }

        [HttpGet("balance")]
        public IActionResult GetBalance()
        {
            var balance = _parkingService.GetBalance();
            return Ok(balance);
        }

        [HttpGet("transaction-log")]
        public IActionResult GetTransactionFromLog()
        {
            var balance = _parkingService.GetTransactionFromLog();
            return Ok(balance);
        }

        [HttpPost("")]
        public IActionResult AddVehicleOnParking([FromBody]VehicleOnParkingModel model)
        {
            if (model.IsParked)
            {
                _parkingService.AddVehicleOnParking(model.VehicleId);
            }
            else
            {
                _parkingService.TakeVehicleWithParking(model.VehicleId);
            }
            return Ok();
        }


    }
}