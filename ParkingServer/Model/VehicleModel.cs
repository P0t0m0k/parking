﻿using Parking.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParkingServer.Model
{
    public class VehicleModel
    {
        public string Id { get; set; }
        public VehicleType Type { get; set; }
        public decimal Balance { get; set; }
    }
}
