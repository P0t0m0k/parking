﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParkingServer.Model
{
    public class ParkingInfoModel
    {
        public int All { get; set; }
        public int Busy { get; set; }
    }
}
