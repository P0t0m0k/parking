﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParkingServer.Model
{
    public class VehicleOnParkingModel
    {
        public string VehicleId {get; set; }
        public bool IsParked {get; set; }
    }
}
