﻿using Parking.Vehicles;
using ParkingServer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace ParkingServer.Services
{
    public class VehiclesService : IVehiclesService
    {
        private readonly static Parking.Parking _parking = Parking.Parking.getInstance();

        public void Create(string id, VehicleType type, decimal balance)
        {
            var vehicle = VehicleCreateService.Create(id, type, balance);
            _parking.AddVehicle(vehicle);
        }

        public void Delete(string id)
        {
            _parking.RemoveVehicle(GetById(id));
        }

        public BaseVehicle[] GetAll()
        {
            return _parking.GetAllVehicle();
        }

        public BaseVehicle GetById(string id)
        {
            return _parking.FindVehicleById(id);
        }
    }
}
