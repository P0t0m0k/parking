﻿using Parking;
using ParkingServer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParkingServer.Services
{
    public class ParkingService: IParkingService
    {
        private readonly static Parking.Parking _parking = Parking.Parking.getInstance();

        public void AddVehicleOnParking(string idVehicle)
        {
            var vehicle = _parking.FindVehicleById(idVehicle);
            _parking.PutOnParking(vehicle);
        }

        public int GetAllParkingSpaces()
        {
            return _parking.GetAllParkingSpaces();
        }

        public decimal GetBalance()
        {
            return _parking.GetBalance();
        }

        public int GetBusyParkingSpaces()
        {
            return _parking.GetBusyParkingSpaces();
        }

        public decimal GetSumFromLastMinute()
        {
            return _parking.GetSumFromLastMinute();
        }

        public List<Transaction> GetTransactionFromLastMinute()
        {
           return _parking.GetTransactionFromLastMinute();
        }

        public string[] GetTransactionFromLog()
        {
           return TransactionLogService.ReadLog();
        }

        public void TakeVehicleWithParking(string idVehicle)
        {
            var vehicle = _parking.FindVehicleById(idVehicle);
            _parking.TakeWithParking(vehicle);
        }
    }
}
